module.exports = {
  "title": "FSCI Services Status",
  "name": "fsci_service_status",
  "description": "Status of services hosted by Free Software Community of India",
  "baseUrl": 'http://status.fsci.in/',
  "defaultLocale": "en",
  "locales": [
    {
      "code": "en",
      "iso": "en-US",
      "name": "English",
      "file": "en.json"
    }
  ],
  "content": {
    "frontMatterFormat": "json",
    "systems": [
      "diaspora",
      "matrix",
      "jitsi-meet",
      "mailing-lists"
    ],
    "head": {
      "link": [
        { rel: 'logo', href: '/icons/img/logo.png', color: '#020c01' }
      ],
      "meta": [{
        'name': "FSCI Service Status",
        'content': "Status of services maintained by FSCI"
      }]
    }
  },
  "theme": {
    "links": {
      "en": {
        home: "https://fsci.in",
        contact: "https://fsci.in/#joinus"
      }
    }
  }
}
